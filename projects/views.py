from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import AddNewProject
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_projects(request):
    proj_list = Project.objects.filter(owner=request.user)
    context = {
        "proj_list": proj_list,
    }
    return render(request, "projects/list.html", context)


@login_required
def proj_detail(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_new_project(request):
    if request.method == "POST":
        form = AddNewProject(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = AddNewProject()
    context = {
        "form": form,
    }
    return render(request, "projects/new_proj.html", context)
