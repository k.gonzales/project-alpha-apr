from django.shortcuts import render, redirect
from tasks.forms import AddTask
from tasks.models import Task
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = AddTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = AddTask()
    context = {
        "form": form,
    }
    return render(request, "tasks/add_task.html", context)


@login_required
def user_task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/user_tasks.html", context)
